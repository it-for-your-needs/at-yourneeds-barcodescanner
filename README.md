# Usage

1. Add it to the Cordova/Ionic project as usual
2. Add `declare var cordova;` in your source file (implies using typescript)
3. Call `cordova.plugins.YNBarcodeScanner.scan( opts, success, err )`
    3.1 `opts` is currently unused. Planned for future purposes
    3.2 `success` and `err` are callback functions for the plugin
    3.3 `success` returns a data parameter including the properties
        3.3.1 `cancelled: boolean`
        3.3.1 `format: string` (can be null)
        3.3.1 `text: string` (can be null) 
    3.4 `err` returns an error object as parameter

If you do any work in the callback which needs some processing power run it with NgZone to prevent performance issues!

For a list of supported formats see [https://github.com/TheLevelUp/ZXingObjC](https://github.com/TheLevelUp/ZXingObjC)

The `format` property of the returned result is according to the official cordova plugin [https://ionicframework.com/docs/native/barcode-scanner/](https://ionicframework.com/docs/native/barcode-scanner/)

## Android Notices

Currently only the Android scanner is able to **pinch to zoom**

### AAR Libraries 

These libraries are compiled from the Android project for https://gitlab.com/it-for-your-needs/barcodescanner including the Pull Request of **TonyTangAndroid**.
For updating these libraries just clone this repository, check out the **vision branch**, open it in Android Studio and let it rebuild the project. You will find the AAR archives in the build folders for the modules.

# Disclaimer

This plugin uses various other open source libraries and resources. 
The according rights belong to their rightful owners.

- https://github.com/dm77/barcodescanner
- including the fork from https://github.com/TonyTangAndroid/barcodescanner
- https://ionicons.com/

# Changelog

## 1.4.1 

- Fixed blurry camera on some devices, especially Huawei phones

## 1.4.0 

- Feature: Updated Android Scanner to use Google Mobile Vision API for scan processing
- Feature: The Android scanner is now able to **pinch to zoom** in the camera view

## 1.3.0

- Fixed: Compatibility with newest Android frameworks
