//
//  YNScanDelegate.swift
//  YNBarcodeScanner
//
//  Created by René Dammerer on 14.05.18.
//

protocol YNScanDelegate {
    func scanResult( result: ZXResult )
    func scanCancelled()
}
