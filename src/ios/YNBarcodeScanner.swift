//
//  YNBarcodeScanner.swift
//  YNBarcodeScanner
//
//  Created by René Dammerer on 14.05.18.
//

@objc(YNBarcodeScanner) class YNBarcodeScanner : CDVPlugin, YNScanDelegate {
    private var scanCommand: CDVInvokedUrlCommand?
    private struct ScannerResult: Codable {
        var cancelled: Bool
        var format: String?
        var text: String?
    }
    
    @objc(scan:)
    func scan(command: CDVInvokedUrlCommand) {
        self.scanCommand = command
        
        // Prevent WebView from crashing
        // self.hasPendingOperation = true

        // Load ViewController from Storyboard
        let storyboard = UIStoryboard(name: "YNScanView", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ScanViewController") as! YNScanViewController
        controller.delegate = self;

        self.viewController?.present(
            controller,
            animated: true,
            completion: nil
        )
    }
    
    // MARK: - Private
    
    private func returnPluginResult( result: ScannerResult ) {
        
        let pluginResult = CDVPluginResult(
            status: CDVCommandStatus_OK,
            messageAs: try! String(data: JSONEncoder().encode(result), encoding: .utf8 )
        )
    
    
        self.commandDelegate!.send(
            pluginResult,
            callbackId: self.scanCommand?.callbackId
        )
    }
    
    // This function maps the ZXBarcodeFormat to strings like the official barcode scanner plugin
    private func barcodeFormatToString(format: ZXBarcodeFormat) -> String {
        switch (format) {
        case kBarcodeFormatAztec:
            return "AZTEC";
            
        case kBarcodeFormatCodabar:
            return "CODABAR";
            
        case kBarcodeFormatCode39:
            return "CODE_39";
            
        case kBarcodeFormatCode93:
            return "CODE_93";
            
        case kBarcodeFormatCode128:
            return "CODE_128";
            
        case kBarcodeFormatDataMatrix:
            return "DATA_MATRIX";
            
        case kBarcodeFormatEan8:
            return "EAN_8";
            
        case kBarcodeFormatEan13:
            return "EAN_13";
            
        case kBarcodeFormatITF:
            return "ITF";
            
        case kBarcodeFormatPDF417:
            return "PDF417";
            
        case kBarcodeFormatQRCode:
            return "QR_CODE";
            
        case kBarcodeFormatRSS14:
            return "RSS14";
            
        case kBarcodeFormatRSSExpanded:
            return "RSS_EXPANDED";
            
        case kBarcodeFormatUPCA:
            return "UPC_A";
            
        case kBarcodeFormatUPCE:
            return "UPC_E";
            
        case kBarcodeFormatUPCEANExtension:
            return "UPC/EAN_EXTENSION";
            
        default:
            return "Unknown";
        }
    }
    
    // MARK: - YNScanDelegate
    
    func scanResult(result: ZXResult) {
        self.returnPluginResult(result: ScannerResult(
            cancelled: false,
            format: self.barcodeFormatToString(format: result.barcodeFormat),
            text: result.text)
        )
    }
    
    func scanCancelled() {
        self.returnPluginResult(result: ScannerResult(
            cancelled: true,
            format: nil,
            text: nil)
        )
    }
}
