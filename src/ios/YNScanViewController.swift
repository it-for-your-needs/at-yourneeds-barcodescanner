//
//  YNScanViewController.swift
//  YNBarcodeScanner
//
//  Created by René Dammerer on 14.05.18.
//

import UIKit

class YNScanViewController: UIViewController, ZXCaptureDelegate, UIBarPositioningDelegate {

    @IBOutlet weak var scanView: UIView!
    @IBOutlet weak var feedView: UIView!
    @IBOutlet weak var navBar: UINavigationBar!
    var capture: ZXCapture = ZXCapture()
    var captureSizeTransform: CGAffineTransform! = nil
    var delegate: YNScanDelegate?
    private var usingBackCam: Bool = true

    deinit {
        self.capture.layer.removeFromSuperlayer()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.capture.camera = self.capture.back()
        self.capture.focusMode = AVCaptureDevice.FocusMode.continuousAutoFocus

        self.feedView.layer.addSublayer(self.capture.layer)
        self.feedView.bringSubviewToFront(self.scanView)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.capture.delegate = self
    }

    // Apply Orientation and size calculation for camera after
    // layout constraints take effect
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.applyOrientation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: nil) { (context) in
            self.applyOrientation()
        }
    }

    // MARK: - UIBarPositioning Delegates

    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.topAttached
    }


    // MARK: - Events

    @IBAction func closeBtnTapped(_ sender: Any) {
        self.capture.stop()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func switchCamera(_ sender: Any) {
        self.capture.stop()

        if self.usingBackCam {
            if ( self.capture.hasFront() ) {
                self.capture.camera = self.capture.front()
                self.usingBackCam = false
            }
        } else {
            if ( self.capture.hasBack() ) {
                self.capture.camera = self.capture.back()
                self.usingBackCam = true
            }
        }

        self.capture.focusMode = AVCaptureDevice.FocusMode.continuousAutoFocus
        self.applyOrientation()
        self.capture.start()
    }

    @IBAction func toggleTorch(_ sender: Any) {
        if self.capture.hasTorch() {
            self.capture.torch = !self.capture.torch
        }
    }

    // MARK: - ZXing Delegates

    func captureResult(_ capture: ZXCapture!, result: ZXResult!) {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)

        self.delegate?.scanResult(result: result)
        self.capture.stop()

        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Private

    // Transform the current view to to the current device rotation
    private func applyOrientation() {
        let orientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        var scanRectRotation: CGFloat
        var captureRotation: Double

        switch (orientation) {
        case UIInterfaceOrientation.portrait:
            captureRotation = 0
            scanRectRotation = 90
            break;
        case UIInterfaceOrientation.landscapeLeft:
            captureRotation = 90
            scanRectRotation = 180
            break;
        case UIInterfaceOrientation.landscapeRight:
            captureRotation = 270
            scanRectRotation = 0
            break;
        case UIInterfaceOrientation.portraitUpsideDown:
            captureRotation = 180
            scanRectRotation = 270
            break;
        default:
            captureRotation = 0
            scanRectRotation = 90
            break;
        }
        self.applyRectOfInterest(orientation: orientation)


        let transform: CGAffineTransform = CGAffineTransform(rotationAngle: CGFloat(captureRotation / 180.0 * .pi))
        self.capture.transform = transform
        self.capture.rotation = scanRectRotation

        self.capture.layer.frame = CGRect(x: 0, y: 0, width: self.feedView.frame.width, height: self.feedView.frame.height)
    }

    // Calculate the area of the scanView in the UI and apply a video capture session onto it
    private func applyRectOfInterest(orientation: UIInterfaceOrientation) {
        var scaleVideoX: CGFloat, scaleVideoY: CGFloat;
        var videoSizeX: CGFloat, videoSizeY: CGFloat;
        var transformedVideoRect: CGRect = self.scanView.frame;

        if self.capture.sessionPreset! == AVCaptureSession.Preset.hd1920x1080.rawValue {
            videoSizeX = 1080.0
            videoSizeY = 1920.0
        } else {
            videoSizeX = 720.0
            videoSizeY = 1280.0
        }


        if orientation.isPortrait {
            scaleVideoX = self.feedView.frame.width / videoSizeX;
            scaleVideoY = self.feedView.frame.height / videoSizeY;

            // Convert CGPoint under portrait mode to map with orientation of image
            // because the image will be cropped before rotate
            // reference: https://github.com/TheLevelUp/ZXingObjC/issues/222
            let realX = transformedVideoRect.origin.y;
            let realY = self.view.frame.size.width - transformedVideoRect.size.width - transformedVideoRect.origin.x;
            let realWidth = transformedVideoRect.size.height;
            let realHeight = transformedVideoRect.size.width;
            transformedVideoRect = CGRect(x: realX, y: realY, width: realWidth, height: realHeight);

        } else {
            scaleVideoX = self.feedView.frame.width / videoSizeY
            scaleVideoY = self.feedView.frame.height / videoSizeX
        }

        self.captureSizeTransform = CGAffineTransform(scaleX: 1.0/scaleVideoX, y: 1.0/scaleVideoY)
        guard let _captureSizeTransform = captureSizeTransform else { return }
        let transformRect = transformedVideoRect.applying(_captureSizeTransform)
        self.capture.scanRect = transformRect
    }


}
