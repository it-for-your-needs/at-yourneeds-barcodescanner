package at.yourneeds.cordova;

import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import me.dm7.barcodescanner.vision.BarcodeFormat;
import me.dm7.barcodescanner.vision.Result;
import me.dm7.barcodescanner.vision.VisionScannerView;

public class YNSimpleScannerActivity extends AppCompatActivity implements VisionScannerView.ResultHandler {
    private static final String TAG = "YNBarcodeScanner";

    private VisionScannerView mScannerView;
    private int mCameraOrientation = 0;
    private boolean mFlashActive = false;
    private boolean mAutoFocus = true;

    private static final String MENU_ITEM_FLASH_RES = "yn_scanner_torch";
    private static final String MENU_ITEM_CAM_TOGGLE_RES = "yn_scanner_camera_toggle";
    private static final String STATE_FLASH_ACTIVE = "flash_active";

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        // Programmatically initialize the scanner view
        mScannerView = new VisionScannerView(this);
        // Set the scanner view as the content view
        setContentView(mScannerView);

        // Default to back and no flash
        this.mCameraOrientation = Camera.CameraInfo.CAMERA_FACING_BACK;

        if (state != null) {
            mFlashActive = state.getBoolean(STATE_FLASH_ACTIVE, false);
        }
        this.mScannerView.setFlash(mFlashActive);
        this.mScannerView.setAutoFocus(mAutoFocus);
        this.mScannerView.setFormats(BarcodeFormat.ALL_FORMATS);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Register ourselves as a handler for scan results.
        mScannerView.setResultHandler(this);
        // Start camera on resume
        mScannerView.startCamera();
        mScannerView.setFlash(mFlashActive);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop camera on pause
        mScannerView.stopCamera();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        // Store current flash state for changes to reenable afterwards
        savedInstanceState.putBoolean(STATE_FLASH_ACTIVE, mFlashActive);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Get resource dynamically as the R constant is always in a project specific Package
        inflater.inflate(getResources().getIdentifier("yn_scanner_menu_items", "menu", getPackageName()), menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int item_id = item.getItemId();
        int MENU_FLASH_ID = getResources().getIdentifier(MENU_ITEM_FLASH_RES, "id", getPackageName());
        int MENU_CAMERA_ID = getResources().getIdentifier(MENU_ITEM_CAM_TOGGLE_RES, "id", getPackageName());

        if ( item_id == MENU_FLASH_ID ) {
            // Toggle flash
            mFlashActive = !mFlashActive;
            this.mScannerView.setFlash(mFlashActive);
            return true;
        } else if ( item_id == MENU_CAMERA_ID ) {
            // Toggle camera
            // TODO: Transit this to the newer hardware API
            this.mScannerView.stopCameraPreview();
            this.mScannerView.stopCamera();

            switch (this.mCameraOrientation) {
                case Camera.CameraInfo.CAMERA_FACING_BACK:
                    this.mCameraOrientation = Camera.CameraInfo.CAMERA_FACING_FRONT;
                    break;
                case Camera.CameraInfo.CAMERA_FACING_FRONT:
                    this.mCameraOrientation = Camera.CameraInfo.CAMERA_FACING_BACK;
                    break;
                default:
                    this.mCameraOrientation = Camera.CameraInfo.CAMERA_FACING_BACK;
            }

            try {
                this.mScannerView.startCamera(this.mCameraOrientation);
                return true;
            } catch (Exception ex) {
                Log.w(TAG, "Camera toggle not possible");
                return false;
            }
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v(TAG, rawResult.getBarcode().displayValue); // Prints scan results
        Log.v(TAG, BarcodeFormat.getFormatById(rawResult.getBarcode().format).toString()); // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
        mScannerView.stopCameraPreview();
        mScannerView.stopCamera();

        // We have our result, go back
        this.finishActivitya(
                rawResult.getBarcode().displayValue,
                String.valueOf(rawResult.getBarcode().format),
                BarcodeFormat.getFormatById(rawResult.getBarcode().format).toString(),
                RESULT_OK
        );
    }

    @Override
    public void onBackPressed() {
        // If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
        mScannerView.stopCameraPreview();
        mScannerView.stopCamera();

        this.finishActivitya("", "", "", RESULT_CANCELED);
    }

    private void finishActivitya(String code, String formatId, String formatName, int resultCode) {
        // We have our result, go back
        Intent data = new Intent();
        data.putExtra("code", code);
        data.putExtra("format_id", formatId);
        data.putExtra("format_name", formatName);
        setResult(resultCode, data);

        finish();
    }

    @Override
    public void finishActivity(int requestCode) {
        super.finishActivity(requestCode);
    }
}
