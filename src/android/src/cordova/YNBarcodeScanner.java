package at.yourneeds.cordova;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

/**
 * This class echoes a string called from JavaScript.
 */
public class YNBarcodeScanner extends CordovaPlugin {

    protected CallbackContext mCallbackContext;

    private static final int BARCODE_CAPTURE_RESULT = 9002;
    private static final int CAMERA_PERMISSION = 1;
    private JSONArray mArgs;

    // public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    //     super.initialize(cordova, webView);
    // }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.mCallbackContext = callbackContext;
        this.mArgs = args;

        PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
        r.setKeepCallback(true);
        callbackContext.sendPluginResult(r);

        if (action.equals("scan")) {

            if ( !this.cordova.hasPermission(Manifest.permission.CAMERA) ) {
                this.cordova.requestPermission(this, CAMERA_PERMISSION, Manifest.permission.CAMERA);
            } else {
                launchActivityAsync();
            }

            return true;
        }
        return false;
    }


    @Override
    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {
        switch (requestCode) {
            case CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchActivityAsync();
                } else {
                    Toast.makeText(cordova.getActivity().getApplicationContext(), "Please grant camera permission for using the scanner", Toast.LENGTH_SHORT).show();
                    Log.d("YNBarcodeScanner", "Permission Denied!");
                    PluginResult result = new PluginResult(PluginResult.Status.ILLEGAL_ACCESS_EXCEPTION);
                    this.mCallbackContext.sendPluginResult(result);
                }
                break;
            default:
                // do nothing
        }
    }

    /**
     * Creates a thread for running the new activity in background
     */
    private void launchActivityAsync() {

        final CordovaPlugin that = this;

        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Intent intent = new Intent(that.cordova.getActivity().getBaseContext(), YNSimpleScannerActivity.class);
                intent.setAction("at.yourneeds.cordova.YNBarcodeScanner.SCAN");
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setPackage(that.cordova.getActivity().getApplicationContext().getPackageName());


                intent.setPackage(that.cordova.getActivity().getApplicationContext().getPackageName());
                //that.cordova.setActivityResultCallback(that);
                that.cordova.startActivityForResult(that, intent, BARCODE_CAPTURE_RESULT);
            }
        });

        // class OneShotTask implements Runnable {
        //     private Context context;
        //     private JSONArray args;
        //     private OneShotTask(Context ctx, JSONArray as) { context = ctx; args = as; }
        //     public void run() {
        //         openNewActivity(context, args);
        //     }
        // }
        // Thread t = new Thread(new OneShotTask(context, this.mArgs));
        // t.start();
    }

    /**
     * Actually opens the activity with options
     */
    // private void openNewActivity(Context context, JSONArray args) {

    //     Intent intent = new Intent(context, YNSimpleScannerActivity.class);

    //     //intent.putExtra("DetectionTypes", args.optInt(0, 1234));
    //     //intent.putExtra("ViewFinderWidth", args.optDouble(1, .5));
    //     //intent.putExtra("ViewFinderHeight", args.optDouble(1, .7));

    //     this.cordova.setActivityResultCallback(this);
    //     this.cordova.startActivityForResult(this, intent, BARCODE_CAPTURE_RESULT);
    // }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_CAPTURE_RESULT && this.mCallbackContext != null) {

            try {
                if (resultCode == Activity.RESULT_OK) {

                    if (data != null) {

                        JSONObject result = new JSONObject();
                        result.put("text", data.getStringExtra("code"));
                        //result.put("format_id", data.getStringExtra("format_id"));
                        result.put("format", data.getStringExtra("format_name"));
                        result.put("cancelled", false);

                        // mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));
                        Log.d("YNBarcodeScanner", "Barcode read: " + data);
                        this.mCallbackContext.success(result);

                    }
                }
                /* TODO: not called correctly as every second scan opening immediately triggers activity result
                else if ( resultCode == Activity.RESULT_CANCELED ) {

                    JSONObject result = new JSONObject();
                    result.put("cancelled", true);
                    mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));

                    Log.d("YNBarcodeScanner", "cancelled: " + result);
                }
                */
            } catch (JSONException e) {
                JSONObject result = new JSONObject();
                try {
                    result.put("cancelled", true);
                } catch (JSONException e1) {
                    // fail silently
                    this.mCallbackContext.error("Failed JSON fallback");
                }
                // mCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, result));
            }
        }
        //  else {
        //     super.onActivityResult(requestCode, resultCode, data);
        // }
    }

    /**
     * This plugin launches an external Activity when the camera is opened, so we
     * need to implement the save/restore API in case the Activity gets killed
     * by the OS while it's in the background.
     */
    @Override
    public void onRestoreStateForActivityResult(Bundle state, CallbackContext callbackContext) {
        this.mCallbackContext = callbackContext;
    }

}
