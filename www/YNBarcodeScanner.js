var exec = cordova.require('cordova/exec');

var PLUGIN_NAME = "YNBarcodeScanner";

/**
 * Constructor.
 *
 * @returns {YNBarcodeScanner}
 */
function YNBarcodeScanner() {}

YNBarcodeScanner.prototype.scan = function(successCallback, errorCallback, config) {

	if (errorCallback == null) {
        errorCallback = function () {
        };
    }

    if (typeof errorCallback != "function") {
        console.log("YNBarcodeScanner.scan failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("YNBarcodeScanner.scan failure: success callback parameter must be a function");
        return;
    }

	exec(
		function(result) {
			try {
				if (typeof result === 'string') {
					successCallback(JSON.parse(result));
				} else {
					successCallback(result);
				}
			} catch( jsonErr ) {
				errorCallback(jsonErr);
			}
		}, function(error) {
			errorCallback(error);
		}, PLUGIN_NAME, 'scan', [config]
	);
}

var scanner = new YNBarcodeScanner();
module.exports = scanner;
